/******************************************************************************
 *
 *  Copyright (C) 2012 Broadcom Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 ******************************************************************************/
package com.broadcom.app.leexplorer;

import android.app.ListActivity;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.broadcom.app.leexplorer.adapters.DeviceAdapter;

public class MainActivity extends ListActivity {
    public static final String TAG = "BtGatt.LeExplorer";

    private GattAppService mService = null;
    private DeviceAdapter mDeviceAdapter;

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
            mService = ((GattAppService.LocalBinder) rawBinder).getService();
            if (!mService.init()) {
                // TODO: Show a notice to the user here....
                finish();
                return;
            }
            mService.scan(true);
            invalidateOptionsMenu();
        }

        @Override
        public void onServiceDisconnected(ComponentName classname) {
            mService = null;
        }
    };

    private final BroadcastReceiver GattDeviceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final BluetoothDevice device = intent.getParcelableExtra(GattAppService.EXTRA_DEVICE);
            final int rssi = intent.getIntExtra(GattAppService.EXTRA_RSSI, 0);
            final int source = intent.getIntExtra(GattAppService.EXTRA_SOURCE, 0);

            if (GattAppService.GATT_DEVICE_FOUND.equals(intent.getAction())) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mDeviceAdapter.addDevice(device, rssi, source);
                    }
                } );

            } else if (GattAppService.GATT_DEVICE_LOST.equals(intent.getAction())) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mDeviceAdapter.removeDevice(device);
                    }
                } );
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDeviceAdapter = new DeviceAdapter(this);
        setListAdapter(mDeviceAdapter);

        Intent bindIntent = new Intent(this, GattAppService.class);
        bindService(bindIntent, mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        menu.getItem(0).setVisible(mService != null ? !mService.isScanning() : false);
        menu.getItem(1).setVisible(mService != null ? mService.isScanning() : false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mService == null) return false;

        switch (item.getItemId()) {
        case R.id.menu_scan:
            mService.scan(true);
            break;

        case R.id.menu_stop:
            mService.scan(false);
            break;

        default:
            break;
        }

        invalidateOptionsMenu();
        return true;
    }

    @Override
    public void onDestroy() {
        unbindService(mServiceConnection);
        mService = null;
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(GattAppService.GATT_DEVICE_FOUND);
        intentFilter.addAction(GattAppService.GATT_DEVICE_LOST);
        registerReceiver(GattDeviceReceiver, intentFilter);

        if (mService != null) mService.scan(true);
        invalidateOptionsMenu();
    }

    @Override
    public void onPause() {
        unregisterReceiver(GattDeviceReceiver);

        if (mService != null) mService.scan(false);
        mDeviceAdapter.clear();
        super.onPause();
    }

    @Override
    public void onListItemClick(ListView list, View view, int position, long id) {
        Intent intent = new Intent(MainActivity.this, DeviceActivity.class);
        intent.putExtra(DeviceActivity.EXTRA_NAME, mDeviceAdapter.getName(position));
        intent.putExtra(DeviceActivity.EXTRA_ADDR, mDeviceAdapter.getAddress(position));
        startActivity(intent);
    }
}
