/******************************************************************************
 *
 *  Copyright (C) 2012 Broadcom Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 ******************************************************************************/
package com.broadcom.app.leexplorer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.ActionBar;
import android.app.ExpandableListActivity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;

import com.broadcom.bt.gatt.BluetoothGattCharacteristic;
import com.broadcom.bt.gatt.BluetoothGattService;

public class DeviceActivity extends ExpandableListActivity {

    public static final String EXTRA_NAME = "NAME";
    public static final String EXTRA_ADDR = "ADDRESS";

    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;
    private static final int STATE_READY = 4;

    private static final String NAME = "NAME";
    private static final String UUID = "UUID";

    private GattAppService mService = null;
    private String mAddress = null;
    private String mName = null;
    private int mState = STATE_DISCONNECTED;
    private SimpleExpandableListAdapter mAdapter;
    List<Map<String, String>> mGroupData = new ArrayList<Map<String, String>>();
    List<List<Map<String, String>>> mChildData = new ArrayList<List<Map<String, String>>>();

    private final BroadcastReceiver GattStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(GattAppService.GATT_CONNECTION_STATE)) {
                Log.d(MainActivity.TAG, "Connection state changed");

                String address = intent.getStringExtra(GattAppService.EXTRA_ADDR);
                if (address.equals(mAddress)) {
                    mState = intent.getBooleanExtra(GattAppService.EXTRA_CONNECTED, false) ?
                            STATE_CONNECTED : STATE_DISCONNECTED;
                    if (mState == STATE_DISCONNECTED) {
                        mGroupData.clear();
                        mChildData.clear();
                    }
                }

            } else if(intent.getAction().equals(GattAppService.GATT_SERVICES_REFRESHED)) {
                Log.d(MainActivity.TAG, "Services refreshed!");

                if (mState == STATE_CONNECTED) mState = STATE_READY;

                mGroupData.clear();
                mChildData.clear();

                AttributeLookup lookup = new AttributeLookup(getApplicationContext());

                List<BluetoothGattService> services = mService.getServices(mAddress);
                for (BluetoothGattService service : services) {
                    Map<String, String> serviceEntry = new HashMap<String, String>();
                    serviceEntry.put(NAME, lookup.getService(service.getUuid()));
                    serviceEntry.put(UUID, service.getUuid().toString());
                    mGroupData.add(serviceEntry);

                    List<BluetoothGattCharacteristic> characteristics = service.getCharacteristics();
                    List<Map<String, String>> children = new ArrayList<Map<String, String>>();

                    for (BluetoothGattCharacteristic characteristic : characteristics) {
                        Map<String, String> childEntry = new HashMap<String, String>();
                        childEntry.put(NAME, lookup.getCharacteristic(characteristic.getUuid()));
                        childEntry.put(UUID, characteristic.getUuid().toString());
                        children.add(childEntry);
                    }

                    mChildData.add(children);
                }
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mAdapter.notifyDataSetChanged();
                    invalidateOptionsMenu();
                }
            } );
        }
    };

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
            mService = ((GattAppService.LocalBinder) rawBinder).getService();
            if (!mService.init()) {
                // TODO: Show a notice to the user here....
                finish();
                return;
            }

            mState = STATE_CONNECTING;
            mService.connect(mAddress);
            invalidateOptionsMenu();
        }

        @Override
        public void onServiceDisconnected(ComponentName classname) {
            mService = null;
        }
    };

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getActionBar();
        if (actionBar!= null) actionBar.setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        if (intent == null) return;

        mAddress = intent.getStringExtra(EXTRA_ADDR);
        mName = intent.getStringExtra(EXTRA_NAME);
        setTitle(mName);

        Intent bindIntent = new Intent(this, GattAppService.class);
        bindService(bindIntent, mServiceConnection, Context.BIND_AUTO_CREATE);

        // Set up our adapter
        mAdapter = new SimpleExpandableListAdapter(
                this,
                mGroupData,
                android.R.layout.simple_expandable_list_item_1,
                new String[] { NAME, UUID },
                new int[] { android.R.id.text1, android.R.id.text2 },
                mChildData,
                R.layout.listitem_char,
                new String[] { NAME, UUID },
                new int[] { R.id.char_name, R.id.char_uuid }
                );
        setListAdapter(mAdapter);
    }

    @Override
    public void onDestroy() {
        unbindService(mServiceConnection);
        mService = null;
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(GattAppService.GATT_CONNECTION_STATE);
        intentFilter.addAction(GattAppService.GATT_SERVICES_REFRESHED);
        registerReceiver(GattStatusReceiver, intentFilter);

        if (mService != null) {
            mState = STATE_CONNECTING;
            mService.connect(mAddress);
            invalidateOptionsMenu();
        }
    }

    @Override
    public void onPause() {
        unregisterReceiver(GattStatusReceiver);
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if (mService != null) mService.disconnect(mAddress);
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_device, menu);

        menu.findItem(R.id.menu_refresh).setVisible(mState == STATE_READY);
        menu.findItem(R.id.menu_connect).setVisible(mState == STATE_DISCONNECTED);
        menu.findItem(R.id.menu_disconnect).setVisible(mState == STATE_CONNECTED || mState == STATE_READY);

        boolean bReconnect = (mService != null && mService.getReconnect(mAddress));
        menu.findItem(R.id.menu_reconnect).setVisible(mState == STATE_READY);
        menu.findItem(R.id.menu_reconnect).setChecked(bReconnect);

        setProgressBarIndeterminateVisibility(mState == STATE_CONNECTING || mState == STATE_CONNECTED);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            if (mService != null) mService.disconnect(mAddress);
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;

        case R.id.menu_refresh:
            if (mService != null) {
                mService.discover(mAddress);
                mState = STATE_CONNECTED;
                invalidateOptionsMenu();
            }
            break;

        case R.id.menu_reconnect:
            if (mService != null) {
                mService.setReconnect(mAddress, !item.isChecked());
                invalidateOptionsMenu();
            }
            break;

        case R.id.menu_connect:
            if (mService != null) {
                mService.connect(mAddress);
                mState = STATE_CONNECTING;
                invalidateOptionsMenu();
            }
            break;

        case R.id.menu_disconnect:
            if (mService != null) mService.disconnect(mAddress);
            break;

        default:
            break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        Map<String, String> service = mGroupData.get(groupPosition);

        List<Map<String, String>> characteristics = mChildData.get(groupPosition);
        Map<String, String> characteristic = characteristics.get(childPosition);

        Intent intent = new Intent(this, CharacteristicActivity.class);
        intent.putExtra(CharacteristicActivity.EXTRA_NAME, mName);
        intent.putExtra(CharacteristicActivity.EXTRA_ADDR, mAddress);
        intent.putExtra(CharacteristicActivity.EXTRA_SERVICE, service.get(UUID));
        intent.putExtra(CharacteristicActivity.EXTRA_CHARACTERISTIC, characteristic.get(UUID));
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

        return super.onChildClick(parent, v, groupPosition, childPosition, id);
    }
}
