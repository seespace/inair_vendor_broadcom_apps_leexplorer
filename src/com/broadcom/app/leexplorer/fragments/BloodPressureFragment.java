/******************************************************************************
 *
 *  Copyright (C) 2012 Broadcom Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 ******************************************************************************/
package com.broadcom.app.leexplorer.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.broadcom.app.leexplorer.DataUtils;
import com.broadcom.app.leexplorer.R;
import com.broadcom.app.leexplorer.ValueFragment;

public class BloodPressureFragment extends ValueFragment {
    private static final int FLAG_UNIT_KPA = 0x01;
    private static final int FLAG_HAS_TIMESTAMP = 0x02;
    private static final int FLAG_HAS_PULSERATE = 0x04;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bloodpressure, container, false);
    }

    @Override
    public void setValue(byte[] value) {
        if (value.length < 7) return;

        int offset = 0;
        int flags = DataUtils.unsignedByteToInt(value[offset++]);

        // Parse data

        Float syst = DataUtils.bytesToFloat(value[offset++], value[offset++]);
        Float diast = DataUtils.bytesToFloat(value[offset++], value[offset++]);
        Float map = DataUtils.bytesToFloat(value[offset++], value[offset++]);

        if ((flags & FLAG_HAS_TIMESTAMP) != 0) offset += 7; // Skip time stamp for now

        // Enable measurement views

        setVisible(R.id.systMeasurement);
        setVisible(R.id.diastMeasurement);
        setVisible(R.id.mapMeasurement);

        // Assign values

        setText(R.id.textSyst, syst.toString());
        setText(R.id.textDiast, diast.toString());
        setText(R.id.textMap, map.toString());

        if (isSet(flags,FLAG_HAS_PULSERATE)) {
            Float pulse = DataUtils.bytesToFloat(value[offset++], value[offset++]);
            setVisible(R.id.pulseMeasurement);
            setText(R.id.textPulse, pulse.toString());
        }

        if (isSet(flags, FLAG_UNIT_KPA)) {
            setText(R.id.textSystUnit, getString(R.string.kPa));
            setText(R.id.textDiastUnit, getString(R.string.kPa));
            setText(R.id.textMapUnit, getString(R.string.kPa));
        }
     }

    private boolean isSet(int flags, int flag) {
        return ((flags & flag) != 0);
    }
}
